package in.docoder.micron.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Author - Vasanth Kumar k
 **/

@RestController
public class TestController {
    @RequestMapping(value = "/ping",method = RequestMethod.GET)
    String pingPing(){
        return "pong";
    }
}
